var vows = require('vows'),
	http = require('http'),
    assert = require('assert'),
    os = require("os"),
    guessMyNumber = require('../server.js');


var HOSTNAME = os.hostname(),
	PORT = 9999;

vows.describe('Server Start').addBatch({
	'Create a new guessMyNumber Server': {
		topic: function() {
			var server = guessMyNumber.createServer({port: PORT});
			return server;
		},
		'returned an object' : function(server) {
			assert.typeOf(server,"object");
		}
	}
}).addBatch({
   	'API Version command': {
        topic: function() {
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/version',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'version number respect format #.#.#[-(Uppercase name)]': function(body) {
				assert.match(body.version, /^[0-9]+\.[0-9]+\.[0-9]+(\-[A-Z]+){0,1}$/);
			}
		}
    }
}).addBatch({
   	'userOne registration': {
        topic: function() {
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/register/userOne',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'name is userOne': function(body) {
				assert.equal(body.name, 'userOne');
			},
			'privateUuid is valid uuid': function(body) {
				assert.match(body.privateUuid,/^[a-f|0-9]{8}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{12}$/,"Incorrect uuid: " + body.privateUuid);
			},
			'Getting registered player': {
				topic: function(body) {return body},
				'gettin player by name:': function(body) {
					assert.isNotNull(body.name);
				},
				'getting player by name': function(body) {
					var player = guessMyNumber.players.get(body.name);
					assert.isNotNull(player, "returned null");
					assert.isObject(player, "returned player is not an object");
					assert.equal(player.name, body.name);
					assert.equal(player.privateUuid, body.privateUuid);
				}
			}
		}
    }
}).addBatch({
   	'userTwo registration': {
        topic: function() {
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/register/userTwo',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'name is userTwo': function(body) {
				assert.equal(body.name, 'userTwo');
			},
			'privateUuid is valid uuid': function(body) {
				assert.match(body.privateUuid,/^[a-f|0-9]{8}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{12}$/,"Incorrect uuid: " + body.privateUuid);
			},
			'Getting registered player': {
				topic: function(body) {return body},
				'gettin player by name:': function(body) {
					assert.isNotNull(body.name);
				},
				'getting player by name': function(body) {
					var player = guessMyNumber.players.get(body.name);
					assert.isNotNull(player, "returned null");
					assert.isObject(player, "returned player is not an object");
					assert.equal(player.name, body.name);
					assert.equal(player.privateUuid, body.privateUuid);
				}
			}
		}
    }
}).addBatch({
   	'Get borard with players One and Two': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is false': function(body) {
				assert.isFalse(body.players[0].numberActivated);
			},
			'body.players[0].numberId is 0': function(body) {
				assert.equal(body.players[0].numberId,0);
			},
			'body.players[0].score is 0': function(body) {
				assert.strictEqual(body.players[0].score,0);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is false': function(body) {
				assert.isFalse(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is 0': function(body) {
				assert.strictEqual(body.me[0].score,0);
			}			
		}
    }
}).addBatch({
   	'Repeated userOne registration': {
        topic: function() {
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/register/userOne',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 520 in response': function(response) {
			assert.equal(response.res.statusCode,520);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'error is 520': function(body) {
				assert.equal(body.error, 520);
			}
		}
    }    
}).addBatch({
   	'Setting number 0000 to userOne': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/' + player.privateUuid + "/0000",
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 528 in response': function(response) {
			assert.equal(response.res.statusCode,528);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned error is 528': function(body) {
				assert.equal(body.error, "528");
			}
		}
    },
   	'Setting number 1111 to userOne': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/' + player.privateUuid + "/1111",
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 528 in response': function(response) {
			assert.equal(response.res.statusCode,528);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned error is 528': function(body) {
				assert.equal(body.error, "528");
			}
		}
    },
   	'Setting number 2245 to userOne': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/' + player.privateUuid + "/2245",
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 528 in response': function(response) {
			assert.equal(response.res.statusCode,528);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned error is 528': function(body) {
				assert.equal(body.error, "528");
			}
		}
    },
   	'Setting number 3334 to userOne': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/' + player.privateUuid + "/3334",
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 528 in response': function(response) {
			assert.equal(response.res.statusCode,528);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned error is 528': function(body) {
				assert.equal(body.error, "528");
			}
		}
    },
   	'Setting number 1255 to userOne': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/' + player.privateUuid + "/1255",
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 528 in response': function(response) {
			assert.equal(response.res.statusCode,528);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned error is 528': function(body) {
				assert.equal(body.error, "528");
			}
		}
    },
   	'Setting number 6576 to userOne': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/' + player.privateUuid + "/6576",
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 528 in response': function(response) {
			assert.equal(response.res.statusCode,528);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned error is 528': function(body) {
				assert.equal(body.error, "528");
			}
		}
    }
}).addBatch({
   	'Setting number 1234 to userOne': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/' + player.privateUuid + "/1234",
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 1234': function(body) {
				assert.equal(body.number, "1234");
			}
		}
    }
}).addBatch({
   	'Get borard with players One and Two after setting user one number': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is false': function(body) {
				assert.isFalse(body.players[0].numberActivated);
			},
			'body.players[0].numberId is 0': function(body) {
				assert.equal(body.players[0].numberId,0);
			},
			'body.players[0].score is 0': function(body) {
				assert.strictEqual(body.players[0].score,0);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -55': function(body) {
				assert.strictEqual(body.me[0].score,-55);
			}			
		}
    }
}).addBatch({
   	'Setting number 1234 to wrong privateUuid': {
        topic: function() {
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/01ab01ab-01ab-01ab-01ab-01ab01ab01ab/1234',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 521 in response': function(response) {
			assert.equal(response.res.statusCode,521);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned error is 521': function(body) {
				assert.equal(body.error, "521");
			}
		}
    }    
}).addBatch({
   	'Setting number 1234 to userOne twice': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/' + player.privateUuid + "/1234",
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 523 in response': function(response) {
			assert.equal(response.res.statusCode,523);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned error is 523': function(body) {
				assert.equal(body.error, "523");
			}
		}
    }
}).addBatch({
   	'Get borard after try setting user one number with one already active': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is false': function(body) {
				assert.isFalse(body.players[0].numberActivated);
			},
			'body.players[0].numberId is 0': function(body) {
				assert.equal(body.players[0].numberId,0);
			},
			'body.players[0].score is 0': function(body) {
				assert.strictEqual(body.players[0].score,0);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -60': function(body) {
				assert.strictEqual(body.me[0].score,-60);
			}			
		}
    }
}).addBatch({
   	'userOne tries to guess his own number': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + player.publicUuid + '/5678',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 529 in response': function(response) {
			assert.equal(response.res.statusCode,529);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 529': function(body) {
				assert.equal(body.error, "529");
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess his own number': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is false': function(body) {
				assert.isFalse(body.players[0].numberActivated);
			},
			'body.players[0].numberId is 0': function(body) {
				assert.equal(body.players[0].numberId,0);
			},
			'body.players[0].score is 0': function(body) {
				assert.strictEqual(body.players[0].score,0);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -565': function(body) {
				assert.strictEqual(body.me[0].score,-565);
			}			
		}
    },
   	'userOne tries to guess number from userTwo (who has no active number)': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/1234',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 527 in response': function(response) {
			assert.equal(response.res.statusCode,527);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 527': function(body) {
				assert.equal(body.error, "527");
			}
		}
    }
}).addBatch({
   	'Get borard after userOne guess try without active number': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is false': function(body) {
				assert.isFalse(body.players[0].numberActivated);
			},
			'body.players[0].numberId is 0': function(body) {
				assert.equal(body.players[0].numberId,0);
			},
			'body.players[0].score is 0': function(body) {
				assert.strictEqual(body.players[0].score,0);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -565': function(body) {
				assert.strictEqual(body.me[0].score,-565);
			}			
		}
    }
}).addBatch({
   	'userTwo (who has no active number) tries to guess number from userOne': {
        topic: function() {
        	var player = guessMyNumber.players.get('userTwo');
        	var oponent = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/1234',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 525 in response': function(response) {
			assert.equal(response.res.statusCode,525);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 525': function(body) {
				assert.equal(body.error, "525");
			}
		}
    },
}).addBatch({
   	'Get borard after userTwo guess try without active number': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userTwo');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user One': function(body) {
	        	var oponent = guessMyNumber.players.get('userOne');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is false': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is 0': function(body) {
				var oponent = guessMyNumber.players.get('userOne');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -565': function(body) {
				assert.strictEqual(body.players[0].score,-565);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user Two': function(body) {
	        	var player = guessMyNumber.players.get('userTwo');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is False': function(body) {
				assert.isFalse(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -5': function(body) {
				assert.strictEqual(body.me[0].score,-5);
			}			
		}
    }
}).addBatch({
   	'userOne tries to guess number with wrong oponent publicUuid': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/01ab01ab-01ab-01ab-01ab-01ab01ab01ab/1234',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 526 in response': function(response) {
			assert.equal(response.res.statusCode,526);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 526': function(body) {
				assert.equal(body.error, "526");
			}
		}
    }
}).addBatch({
   	'Get borard after userOne guess try with wrong oponent publicUuid': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is false': function(body) {
				assert.isFalse(body.players[0].numberActivated);
			},
			'body.players[0].numberId is 0': function(body) {
				assert.equal(body.players[0].numberId,0);
			},
			'body.players[0].score is -5': function(body) {
				assert.strictEqual(body.players[0].score,-5);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -570': function(body) {
				assert.strictEqual(body.me[0].score,-570);
			}			
		}
    }
}).addBatch({
   	'userOne tries to guess number with wrong privateUuid': {
        topic: function() {
        	var oponent = guessMyNumber.players.get('userOne');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/01ab01ab-01ab-01ab-01ab-01ab01ab01ab/' + oponent.publicUuid + '/1234',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 521 in response': function(response) {
			assert.equal(response.res.statusCode,521);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 521': function(body) {
				assert.equal(body.error, "521");
			}
		}
    }
}).addBatch({
   	'Setting number 5678 to userTwo': {
        topic: function() {
        	var player = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/' + player.privateUuid + "/5678",
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 1234': function(body) {
				assert.equal(body.number, "5678");
			}
		}
    }
}).addBatch({
   	'Get borard after userTwo sets an active number': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userTwo');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user One': function(body) {
	        	var oponent = guessMyNumber.players.get('userOne');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is false': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is 0': function(body) {
				var oponent = guessMyNumber.players.get('userOne');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -570': function(body) {
				assert.strictEqual(body.players[0].score,-570);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user Two': function(body) {
	        	var player = guessMyNumber.players.get('userTwo');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is True': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -30': function(body) {
				assert.strictEqual(body.me[0].score,-30);
			}			
		}
    }
}).addBatch({
   	'userOne tries to guess 1234 number from userTwo': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/1234',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 1234': function(body) {
				assert.equal(body.number, "1234");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 0': function(body) {
				assert.equal(body.correctChars, 0);
			},
			'Returned existingChars is 0': function(body) {
				assert.equal(body.existingChars, 0);
			},
			'Returned wrongChars is 4': function(body) {
				assert.equal(body.wrongChars, 4);
			}
		}
    }
}).addBatch({
   	'Get borard after tries to guess 1234 number from userTwo': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -29': function(body) {
				assert.strictEqual(body.players[0].score,-29);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -575': function(body) {
				assert.strictEqual(body.me[0].score,-575);
			}			
		}
    }
}).addBatch({
   	'userOne tries to guess 1234 number from userTwo without waiting for next attemp': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/1234',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 524 in response': function(response) {
			assert.equal(response.res.statusCode,524);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned error number is 524': function(body) {
				assert.equal(body.error, "524");
			},
			'Returned minInterval is 1000': function(body) {
				assert.equal(body.minInterval, 1000);
			},
			'Returned timeToNextAttemp is less than 1000': function(body) {
				assert.strictEqual(body.timeToNextAttemp < 1000, true);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 1234 number from userTwo without waiting for next attemp': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -29': function(body) {
				assert.strictEqual(body.players[0].score,-29);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -580': function(body) {
				assert.strictEqual(body.me[0].score,-580);
			}			
		}
    }
}).addBatch({
   	'userOne tries to guess 5769 number from userTwo after waiting 1000ms': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;
        	setTimeout(function() {
	            http.request({
				  hostname: HOSTNAME,
				  port: PORT,
				  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/5769',
				  method: 'GET'
				}, function(res) {
					var body = "";
					res.on('data', function (chunk) {
						body += chunk;
					});
					res.on('end', function () {
					  	callback(null, {res: res, body: body});					
					})
				}).on('error', function(e) {
				  	callback(null, e);
				}).end();
        	}, 1000);
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 5769': function(body) {
				assert.equal(body.number, "5769");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 1': function(body) {
				assert.equal(body.correctChars, 1);
			},
			'Returned existingChars is 2': function(body) {
				assert.equal(body.existingChars, 2);
			},
			'Returned wrongChars is 4': function(body) {
				assert.equal(body.wrongChars, 1);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 5769 number from userTwo after waiting 1000ms': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -28': function(body) {
				assert.strictEqual(body.players[0].score,-28);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -585': function(body) {
				assert.strictEqual(body.me[0].score,-585);
			}			
		}
    }
}).addBatch({
   	'userOne tries to guess 0000 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;

        	//Reseting wait time por userOne
        	player.lastGuessAttemp = 0;

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/0000',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();

        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 0000': function(body) {
				assert.equal(body.number, "0000");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 0': function(body) {
				assert.equal(body.correctChars, 0);
			},
			'Returned existingChars is 0': function(body) {
				assert.equal(body.existingChars, 0);
			},
			'Returned wrongChars is 4': function(body) {
				assert.equal(body.wrongChars, 4);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 0000 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -27': function(body) {
				assert.strictEqual(body.players[0].score,-27);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -590': function(body) {
				assert.strictEqual(body.me[0].score,-590);
			}			
		}
    }
}).addBatch({
   	'userOne tries to guess 1111 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;

        	//Reseting wait time por userOne
        	player.lastGuessAttemp = 0;

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/1111',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();

        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 1111': function(body) {
				assert.equal(body.number, "1111");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 0': function(body) {
				assert.equal(body.correctChars, 0);
			},
			'Returned existingChars is 0': function(body) {
				assert.equal(body.existingChars, 0);
			},
			'Returned wrongChars is 4': function(body) {
				assert.equal(body.wrongChars, 4);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 1111 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -26': function(body) {
				assert.strictEqual(body.players[0].score,-26);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -595': function(body) {
				assert.strictEqual(body.me[0].score,-595);
			}			
		}
    }
}).addBatch({    
   	'userOne tries to guess 5555 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;

        	//Reseting wait time por userOne
        	player.lastGuessAttemp = 0;

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/5555',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();

        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 5555': function(body) {
				assert.equal(body.number, "5555");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 1': function(body) {
				assert.equal(body.correctChars, 1);
			},
			'Returned existingChars is 3': function(body) {
				assert.equal(body.existingChars, 3);
			},
			'Returned wrongChars is 0': function(body) {
				assert.equal(body.wrongChars, 0);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 5555 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -25': function(body) {
				assert.strictEqual(body.players[0].score,-25);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -600': function(body) {
				assert.strictEqual(body.me[0].score,-600);
			}			
		}
    }
}).addBatch({    
   	'userOne tries to guess 5566 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;

        	//Reseting wait time por userOne
        	player.lastGuessAttemp = 0;

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/5566',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();

        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 5566': function(body) {
				assert.equal(body.number, "5566");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 1': function(body) {
				assert.equal(body.correctChars, 1);
			},
			'Returned existingChars is 3': function(body) {
				assert.equal(body.existingChars, 3);
			},
			'Returned wrongChars is 0': function(body) {
				assert.equal(body.wrongChars, 0);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 5566 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -24': function(body) {
				assert.strictEqual(body.players[0].score,-24);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -605': function(body) {
				assert.strictEqual(body.me[0].score,-605);
			}			
		}
    }
}).addBatch({    
   	'userOne tries to guess 8765 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;

        	//Reseting wait time por userOne
        	player.lastGuessAttemp = 0;

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/8765',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();

        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 8765': function(body) {
				assert.equal(body.number, "8765");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 0': function(body) {
				assert.equal(body.correctChars, 0);
			},
			'Returned existingChars is 4': function(body) {
				assert.equal(body.existingChars, 4);
			},
			'Returned wrongChars is 0': function(body) {
				assert.equal(body.wrongChars, 0);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 8765 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -23': function(body) {
				assert.strictEqual(body.players[0].score,-23);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -610': function(body) {
				assert.strictEqual(body.me[0].score,-610);
			}			
		}
    }
}).addBatch({    
   	'userOne tries to guess 5000 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;

        	//Reseting wait time por userOne
        	player.lastGuessAttemp = 0;

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/5000',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();

        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 5000': function(body) {
				assert.equal(body.number, "5000");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 1': function(body) {
				assert.equal(body.correctChars, 1);
			},
			'Returned existingChars is 0': function(body) {
				assert.equal(body.existingChars, 0);
			},
			'Returned wrongChars is 3': function(body) {
				assert.equal(body.wrongChars, 3);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 5000 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -22': function(body) {
				assert.strictEqual(body.players[0].score,-22);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -615': function(body) {
				assert.strictEqual(body.me[0].score,-615);
			}			
		}
    }
}).addBatch({    
   	'userOne tries to guess 0600 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;

        	//Reseting wait time por userOne
        	player.lastGuessAttemp = 0;

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/0600',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();

        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 0600': function(body) {
				assert.equal(body.number, "0600");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 1': function(body) {
				assert.equal(body.correctChars, 1);
			},
			'Returned existingChars is 0': function(body) {
				assert.equal(body.existingChars, 0);
			},
			'Returned wrongChars is 3': function(body) {
				assert.equal(body.wrongChars, 3);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 0600 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -21': function(body) {
				assert.strictEqual(body.players[0].score,-21);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -620': function(body) {
				assert.strictEqual(body.me[0].score,-620);
			}			
		}
    }
}).addBatch({    
   	'userOne tries to guess 0678 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;

        	//Reseting wait time por userOne
        	player.lastGuessAttemp = 0;

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/0678',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();

        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 0678': function(body) {
				assert.equal(body.number, "0678");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 3': function(body) {
				assert.equal(body.correctChars, 3);
			},
			'Returned existingChars is 0': function(body) {
				assert.equal(body.existingChars, 0);
			},
			'Returned wrongChars is 1': function(body) {
				assert.equal(body.wrongChars, 1);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 0678 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is True': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -20': function(body) {
				assert.strictEqual(body.players[0].score,-20);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -625': function(body) {
				assert.strictEqual(body.me[0].score,-625);
			}			
		}
    }
}).addBatch({    
   	'userOne tries to guess 5678 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var player = guessMyNumber.players.get('userOne');
        	var oponent = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;

        	//Reseting wait time por userOne
        	player.lastGuessAttemp = 0;

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/guessnumber/' + player.privateUuid + '/' + oponent.publicUuid + '/5678',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();

        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 5678': function(body) {
				assert.equal(body.number, "5678");
			},
			'Returned numberId is valid': function(body) {
				assert.match(body.numberId,/^[a-f|0-9]{32}$/,"Incorrect numberId format: " + body.numberId);
			},
			'Returned correctChars is 4': function(body) {
				assert.equal(body.correctChars, 4);
			},
			'Returned existingChars is 0': function(body) {
				assert.equal(body.existingChars, 0);
			},
			'Returned wrongChars is 0': function(body) {
				assert.equal(body.wrongChars, 0);
			}
		}
    }
}).addBatch({
   	'Get borard after userOne tries to guess 5678 number from userTwo overriding min attemp time restriction': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userOne');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user Two': function(body) {
	        	var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is False': function(body) {
				assert.isFalse(body.players[0].numberActivated);
			},
			'body.players[0].numberId is userTwo': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].numberId is not 0': function(body) {
				var oponent = guessMyNumber.players.get('userTwo');
				assert.notEqual(body.players[0].numberId,0);
			},
			'body.players[0].score is -20': function(body) {
				assert.strictEqual(body.players[0].score,-20);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user One': function(body) {
	        	var player = guessMyNumber.players.get('userOne');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is true': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -425': function(body) {
				assert.strictEqual(body.me[0].score,-425);
			}			
		}
    }
}).addBatch({
   	'Setting number 6789 to userTwo': {
        topic: function() {
        	var player = guessMyNumber.players.get('userTwo');
        	var callback = this.callback;
            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/play/setnumber/' + player.privateUuid + "/6789",
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned number is 6789': function(body) {
				assert.equal(body.number, "6789");
			}
		}
    }
}).addBatch({
   	'Get borard after userTwo sets an active number': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userTwo');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/players/board/' + player.privateUuid,
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'body.players is an array': function(body) {
				assert.isArray(body.players);
			},
			'body.players is an array with one item': function(body) {
				assert.equal(body.players.length, 1);
			},
			'body.players[0] array item is user One': function(body) {
	        	var oponent = guessMyNumber.players.get('userOne');
				assert.equal(body.players[0].publicUuid, oponent.publicUuid);
			},
			'body.players[0] has no privateUuid': function(body) {
				assert.isUndefined(body.players[0].privateUuid);
			},
			'body.players[0].numberActivated is false': function(body) {
				assert.isTrue(body.players[0].numberActivated);
			},
			'body.players[0].numberId is 0': function(body) {
				var oponent = guessMyNumber.players.get('userOne');
				assert.equal(body.players[0].numberId,oponent.numberId);
			},
			'body.players[0].score is -425': function(body) {
				assert.strictEqual(body.players[0].score,-425);
			},
			'body.me is an array': function(body) {
				assert.isArray(body.me);
			},
			'body.me is an array with one item': function(body) {
				assert.equal(body.me.length, 1);
			},
			'body.me array[0] item is user Two': function(body) {
	        	var player = guessMyNumber.players.get('userTwo');
				assert.equal(body.me[0].privateUuid, player.privateUuid);
			},
			'body.me[0] has no publicUuid': function(body) {
				assert.isUndefined(body.me[0].publicUuid);
			},
			'body.me[0].numberActivated is True': function(body) {
				assert.isTrue(body.me[0].numberActivated);
			},
			'body.me[0].numberId is undefined': function(body) {
				assert.isUndefined(body.me[0].numberId);
			},
			'body.me[0].score is -45': function(body) {
				assert.strictEqual(body.me[0].score,-45);
			}			
		}
    }
}).addBatch({
   	'Get admin borard with wrong password': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userTwo');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/admin/board/4321',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 401 in response': function(response) {
			assert.equal(response.res.statusCode,401);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {return JSON.parse(response.body)},
			'Returned error number is 401': function(response) {
				assert.equal(response.error, "401");
			},
			'Returnde message has no players item' : function(response) {
				assert.isUndefined(response.body);
			}
		}
    }
}).addBatch({
   	'Get admin borard': {
        topic: function() {
        	var callback = this.callback;
        	var player = guessMyNumber.players.get('userTwo');

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/admin/board/1234',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body is a Json': function(response) {
			assert.doesNotThrow(function() {JSON.parse(response.body)},/.*/, "Canot parse as JSON: " + response.body);
		},
		'Checking body response': {
			topic: function(response) {
				var userOneIndex = -1,
					userTwoIndex = -1,
					players = JSON.parse(response.body).players;

				for(var i = 0; i < players.length; i++) {
					if(players[i].name == "userOne") userOneIndex = i;
					if(players[i].name == "userTwo") userTwoIndex = i;
				}

				return {body: JSON.parse(response.body), userOneIndex: userOneIndex, userTwoIndex: userTwoIndex};
			},
			'body.players is an array': function(response) {
				assert.isArray(response.body.players);
			},
			'body.players is an array with 2 item': function(response) {
				assert.equal(response.body.players.length, 2);
			},
			'body.players item 0 is userOne': function(response) {
				assert.equal(response.body.players[response.userOneIndex].name, "userOne");
			},
			'body.players item 2 is userTwo': function(response) {
				assert.equal(response.body.players[response.userTwoIndex].name, "userTwo");
			},
			'body.players 0 and 1 have valid ips': function(response) {
				assert.match(response.body.players[response.userOneIndex].userIp,/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/,"userOne");
				assert.match(response.body.players[response.userTwoIndex].userIp,/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/,"userTwo");
			},
			'body.players 0 and 1 have valid private uuid': function(response) {
				assert.match(response.body.players[response.userOneIndex].privateUuid,/^[a-f|0-9]{8}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{12}$/,"userOne");
				assert.match(response.body.players[response.userTwoIndex].privateUuid,/^[a-f|0-9]{8}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{12}$/,"userTwo");
			},
			'body.players 0 and 1 have valid public uuid': function(response) {
				assert.match(response.body.players[response.userOneIndex].publicUuid,/^[a-f|0-9]{8}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{12}$/,"userOne");
				assert.match(response.body.players[response.userTwoIndex].publicUuid,/^[a-f|0-9]{8}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{12}$/,"userTwo");
			},
			'body.players 0 and 1 have valid numbers': function(response) {
				assert.match(response.body.players[response.userOneIndex].number,/^[0-9]{4}$/,"userOne");
				assert.match(response.body.players[response.userTwoIndex].number,/^[0-9]{4}$/,"userTwo");
			},
			'body.players 0 and 1 have valid numbers': function(response) {
				assert.match(response.body.players[response.userOneIndex].numberId,/^[a-f|0-9]{32}$/,"userOne");
				assert.match(response.body.players[response.userTwoIndex].numberId,/^[a-f|0-9]{32}$/,"userTwo");
			},
			'body.players 0 and 1 have active numbers': function(response) {
				assert.isTrue(response.body.players[response.userOneIndex].activeNumber,"userOne");
				assert.isTrue(response.body.players[response.userTwoIndex].activeNumber,"userTwo");
			},
			'body.players 0 last attemp greater than 0': function(response) {
				assert.isTrue(response.body.players[response.userOneIndex].lastGuessAttemp>0,"userOne");
			},
			'body.players 1 last attemp is 0': function(response) {
				assert.equal(response.body.players[response.userTwoIndex].lastGuessAttemp,0,"userTwo");
			},
			'body.players 0 last score is -425': function(response) {
				assert.equal(response.body.players[response.userOneIndex].score,-425);
			},
			'body.players 1 last score is -45': function(response) {
				assert.equal(response.body.players[response.userTwoIndex].score,-45);
			},
			'body.players 0 stats.setedNumbers': function(response) {
				assert.deepEqual(response.body.players[response.userOneIndex].stats.setedNumbers,{ quantity: 1, score: -25 });
			},
			'body.players 0 stats.guessedNumbers': function(response) {
				assert.deepEqual(response.body.players[response.userOneIndex].stats.guessedNumbers,{ quantity: 1, score: 200 });
			},
			'body.players 0 stats.wrongGuessedNumbers': function(response) {
				assert.deepEqual(response.body.players[response.userOneIndex].stats.wrongGuessedNumbers,{ quantity: 10, score: -50 });
			},
			'body.players 0 stats.wrongGuessNumbersReceived': function(response) {
				assert.deepEqual(response.body.players[response.userOneIndex].stats.wrongGuessNumbersReceived,{ quantity: 0, score: 0 });
			},
			'body.players 0 stats.wastedRequests': function(response) {
				assert.deepEqual(response.body.players[response.userOneIndex].stats.wastedRequests,{ quantity: 11, score: -550 });
			},
			'body.players 1 stats.setedNumbers': function(response) {
				assert.deepEqual(response.body.players[response.userTwoIndex].stats.setedNumbers,{ quantity: 2, score: -50 });
			},
			'body.players 1 stats.guessedNumbers': function(response) {
				assert.deepEqual(response.body.players[response.userTwoIndex].stats.guessedNumbers,{ quantity: 0, score: 0 });
			},
			'body.players 1 stats.wrongGuessedNumbers': function(response) {
				assert.deepEqual(response.body.players[response.userTwoIndex].stats.wrongGuessedNumbers,{ quantity: 0, score: 0 });
			},
			'body.players 1 stats.wrongGuessNumbersReceived': function(response) {
				assert.deepEqual(response.body.players[response.userTwoIndex].stats.wrongGuessNumbersReceived,{ quantity: 10, score: 10 });
			},
			'body.players 1 stats.wastedRequests': function(response) {
				assert.deepEqual(response.body.players[response.userTwoIndex].stats.wastedRequests,{ quantity: 1, score: -5 });
			},
			'body.players 0 requests.first > 0': function(response) {
				assert.isTrue(response.body.players[response.userOneIndex].requests.first > 0);
			},
			'body.players 0 requests.last > 0': function(response) {
				assert.isTrue(response.body.players[response.userOneIndex].requests.last > 0);
			},
			'body.players 0 requests.first < requests.last': function(response) {
				assert.isTrue(response.body.players[response.userOneIndex].requests.first < response.body.players[response.userOneIndex].requests.last);
			},
			'body.players 0 requests.quantity = 42': function(response) {
				assert.equal(response.body.players[response.userOneIndex].requests.quantity, 42);
			},
			'body.players 1 requests.first > 0': function(response) {
				assert.isTrue(response.body.players[response.userTwoIndex].requests.first > 0);
			},
			'body.players 1 requests.last > 0': function(response) {
				assert.isTrue(response.body.players[response.userTwoIndex].requests.last > 0);
			},
			'body.players 1 requests.first < requests.last': function(response) {
				assert.isTrue(response.body.players[response.userTwoIndex].requests.first < response.body.players[response.userTwoIndex].requests.last);
			},
			'body.players 1 requests.quantity = 7': function(response) {
				assert.equal(response.body.players[response.userTwoIndex].requests.quantity, 7);
			},
			'body.stats is an object': function(response) {
				assert.isObject(response.body.stats);
			},
			'body.stats.seccond30 is greater than 0': function(response) {
				assert.isTrue(response.body.stats.seccond30 > 0);
			},
			'body.stats.minute1 is greater than 0': function(response) {
				assert.isTrue(response.body.stats.minute1 > 0);
			},
			'body.stats.minute5 is greater than 0': function(response) {
				assert.isTrue(response.body.stats.minute5 > 0);
			},
			'body.stats.minute10 is greater than 0': function(response) {
				assert.isTrue(response.body.stats.minute10 > 0);
			},
			'body.stats.minute30 is greater than 0': function(response) {
				assert.isTrue(response.body.stats.minute30 > 0);
			},
			'body.stats.hour1 is greater than 0': function(response) {
				assert.isTrue(response.body.stats.hour1 > 0);
			},
			'body.stats.hour3 is greater than 0': function(response) {
				assert.isTrue(response.body.stats.hour3 > 0);
			},
			'body.stats.hour6 is greater than 0': function(response) {
				assert.isTrue(response.body.stats.hour6 > 0);
			},
			'body.stats.hour12 is greater than 0': function(response) {
				assert.isTrue(response.body.stats.hour12 > 0);
			},
			'body.stats.hour24 is greater than 0': function(response) {
				assert.isTrue(response.body.stats.hour24 > 0);
			},
			'body.stats.total is 55': function(response) {
				assert.equal(response.body.stats.total, 55);
			},
			'body.stats.startTracking is greater than 0': function(response) {
				assert.isTrue(response.body.stats.startTracking > 0);
			},
			'body.stats.serverTime is greater than 0': function(response) {
				assert.isTrue(response.body.stats.serverTime > 0);
			}
		}
    }
}).addBatch({
   	'Get Api Documentation': {
        topic: function() {
        	var callback = this.callback;

            http.request({
			  hostname: HOSTNAME,
			  port: PORT,
			  path: '/doc',
			  method: 'GET'
			}, function(res) {
				var body = "";
				res.on('data', function (chunk) {
					body += chunk;
				});
				res.on('end', function () {
				  	callback(null, {res: res, body: body});					
				})
			}).on('error', function(e) {
			  	callback(null, e);
			}).end();
        },
        'return http status 200 in response': function(response) {
			assert.equal(response.res.statusCode,200);
		},
        'response body contains "API Specification Doc"': function(response) {
			assert.match(response.body, /API Specification Doc/, "Text not found in response.");
		}
    }
}).addBatch({
	'Server Stop': {
		topic: function() {
			guessMyNumber.server.close(function() {
				return function(callback) {callback(null, true)};
			})(this.callback);
		},
		'fires close callback' : function(closed) {
			assert.isTrue(true);
			assert(closed);
			console.log("Listo")
		}
	}
}).export(module);

