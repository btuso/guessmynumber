var http = require('http'),
    winston = require('winston'),
    journey = require('journey'),
    uuid = require('node-uuid'),
    crypto = require('crypto'),
    fs = require("fs");

var API_VERSION = "1.0.2";

var secret = Math.random();

var options = {
  numberLength: 4,
  minRetryTime: 1000,
  adminPassword: 1234,
  port: process.env.VCAP_APP_PORT || 9999,
  maxIldeTime: 1000 * 60 * 30
}

var errorCodes = {
  "401":"Admin password is incorrect.",
  "520":"User Already registered.",
  "521":"Private UUID is was not found.",
  "523":"Private UUID allready has an active number.",
  "524":"Minimum interval between attemps was not expired.",
  "525":"Private UUID has no active number.",
  "526":"Public UUID is was not found.",
  "527":"Public UUID has no active number.",
  "528":"Invalid number.",
  "529":"User tries to guess his own number."

}

var timer = function() {
  var getActualTime = function() {
    var now = new Date();
    return now.getTime();
  }

  var register = function() {
    now = getActualTime();
    stats.total++;
    var diff = now - stats.lastRequest,
        diffMinute = now - stats.lastRequestMinute,
        newSlots = 0;
    
    if(diff < 1000 * 60) {
      stats.daySlots[0]++;

      if(diffMinute < 1000) {
        stats.minuteSlots[0]++;
      } else {
        newSlots = Math.floor(diffMinute / 1000);
        for(var i = 0; i < newSlots; i++) {
          stats.minuteSlots.pop();
          stats.minuteSlots.unshift(0);
        }
        stats.minuteSlots[0]++;
        stats.lastRequestMinute = now;
      }
    } else {
      newSlots = Math.floor(diff / (1000 * 60));
      for(var i = 0; i < newSlots; i++) {
        stats.daySlots.pop();
        stats.daySlots.unshift(0);
      }
      stats.daySlots[0]++;
      stats.lastRequest = now;
    }
  }

  var getSlots = function(){
    if(stats.minuteSlots[0]>0) 
      stats.minuteSlots[0]--;
    if(stats.daySlots[0]>0) 
      stats.daySlots[0]--;

    return {
       "Seconds" : stats.minuteSlots,
       "Minutes" : stats.daySlots
    }
  }

  var getStats = function() {
    var seccond30 = 0,
        minute1 = 0,
        minute5 = 0,
        minute10 = 0,
        minute30 = 0,
        hour1 = 0,
        hour3 = 0,
        hour6 = 0,
        hour12 = 0,
        hour24 = 0;

    for(var i = 0; i < stats.minuteSlots.length; i++) {
      if(i < 30) {
        seccond30+=stats.minuteSlots[i];
      }
      if(i < 60) {
        minute1+=stats.minuteSlots[i];
      }
    }



    for(var i = 0; i < stats.daySlots.length; i++) {
      if(i < 5) {
        minute5+=stats.daySlots[i];
      }
      if(i < 10) {
        minute10+=stats.daySlots[i];
      }
      if(i < 30) {
        minute30+=stats.daySlots[i];
      }
      if(i <= 60) {
        hour1+=stats.daySlots[i];
      }
      if(i <= 60 * 3) {
        hour3+=stats.daySlots[i];
      }
      if(i <= 60 * 6) {
        hour6+=stats.daySlots[i];
      }
      if(i <= 60 * 12) {
        hour12+=stats.daySlots[i];
      }
      if(i <= 60 * 24) {
        hour24+=stats.daySlots[i];
      }
    }

    return {
      seccond30:seccond30,
      minute1:minute1,
      minute5:minute5,
      minute10:minute10,
      minute30:minute30,
      hour1:hour1,
      hour3:hour3,
      hour6:hour6,
      hour12:hour12,
      hour24:hour24,
      total:stats.total,
      startTracking: getActualTime() - stats.startTracking,
      serverTime: getActualTime()
    };
  }

  var stats = {
    total: 0,
    daySlots: [],
    minuteSlots: [],
    lastRequest: getActualTime(),
    lastRequestMinute: getActualTime(),
    startTracking: getActualTime()
  };

  for(var i = 0; i<(60*24); i++) {
    stats.daySlots[i] = 0;
  }

  for(var i = 0; i<60; i++) {
    stats.minuteSlots[i] = 0;
  }



  return {
    get: getActualTime,
    timeToNextAttemp: function(last, minDiff) {
      var now = getActualTime();
      return ((now - last - minDiff) * -1);
    },
    register: register,
    getStats: getStats,
    getSlots: getSlots
  }
}();

var players = function () {
  var players = [],
      lastPurge = timer.get();

  var getPlayer = function(pattern) {
    for(var i = 0; i < players.length; i++) {
      if(pattern == players[i].name || pattern == players[i].privateUuid || pattern == players[i].publicUuid) {
        return players[i];
      }
    }
    return null;
  }

  return {
    board: function(privateUuid) {
      var playerFound = false,
          message = {players: [], me: []},
          now = timer.get();

      if(now - lastPurge > options.maxIldeTime) {
        for(var i = 0; i < players.length; i++) {
          if(now - players[i].requests.last > options.maxIldeTime) players.splice(i,1);
        }
      }

      var player = getPlayer(privateUuid);

      if(player != null) {
        var playerFound = true;

        player.requests.quantity++;
        player.requests.last = timer.get();

        var sortedPlayer = JSON.parse(JSON.stringify(players));
        sortedPlayer.sort(function(a,b) {
          var aId, bId;
          aId = parseInt(a.privateUuid.split("-")[0],16) ^ parseInt(privateUuid.split("-")[0],16);
          bId = parseInt(b.privateUuid.split("-")[0],16) ^ parseInt(privateUuid.split("-")[0],16);
          return bId - aId;
        });
        for(var i=0; i<sortedPlayer.length; i++) {
          if(sortedPlayer[i].privateUuid == privateUuid) {
            message.me.push({privateUuid: sortedPlayer[i].privateUuid, numberActivated: sortedPlayer[i].activeNumber, score: sortedPlayer[i].score});
          } else {
            message.players.push({publicUuid: sortedPlayer[i].publicUuid, numberActivated: sortedPlayer[i].activeNumber, numberId: sortedPlayer[i].numberId, score: sortedPlayer[i].score});
          }
          
        }
      }

      if(playerFound) {
        message = message;
        status = 200;
      } else {
        status = "521";
        message = {error: status, message: errorCodes[status]};
      }

      return {
        status: status,
        message: message
      }
    },
    register: function(name, userIp) {
      var privateUuid = uuid.v4(),
          publicUuid = uuid.v4(),
          allreadyRegistered = false,
          status = 0,
          player = {},
          message = {};

      if(getPlayer(name) != null) allreadyRegistered = true;
      
      if(!allreadyRegistered) {
        player = {
          name: name, 
          userIp: userIp, 
          privateUuid: privateUuid, 
          publicUuid: publicUuid,
          number: "0000",
          numberId: "0",
          activeNumber: false,
          lastGuessAttemp: 0,
          requests: {first: timer.get(), last: timer.get(), quantity: 1},
          score: 0,
          stats: {
            setedNumbers: {quantity: 0, score: 0},
            guessedNumbers: {quantity: 0, score: 0},
            wrongGuessedNumbers: {quantity: 0, score: 0},
            wrongGuessNumbersReceived: {quantity: 0, score: 0},
            wastedRequests: {quantity: 0, score: 0}
          }
        };

        players.push(player);
        message = {
          name: name,
          privateUuid: privateUuid
        };

        status = 200;
      } else {
        status = "520";
        message = {error: status, message: errorCodes[status]};
      }
          
      return {
        status: status,
        message: message
      }
    },
    get: function(pattern) {
      return getPlayer(pattern);
    },
    reset: function(adminPassword) {
      if(adminPassword === options.adminPassword) {
        winston.info('Restarting Players');
        players = [];
        return {
          status: 200,
          message: {message: "Players Restarted"}
        }
      } else {
        return {
          status: "401",
          message: {error: "401", message: errorCodes["401"]}
        }
      }
    },
    getAll: function(adminPassword) {
      if(adminPassword === options.adminPassword) {
        return {
          status: 200,
          message: {players:players}
        }
      } else {
        return {
          status: "401",
          message: {error: "401", message: errorCodes["401"]}
        }
      }
    }
  }
}();

var admin = function() {
  return {
    reset: function(adminPassword) {
        return players.reset(adminPassword);
    },
    board: function(adminPassword) {
      var msg = JSON.parse(JSON.stringify(players.getAll(adminPassword)));
      if(msg.status == "200") {
        var aPlayers = msg.message.players;
        aPlayers.sort(function(a,b) {
          if(a.score != b.score) {
            return b.score - a.score;
          } else {
            var aId, bId;
            aId = parseInt(a.privateUuid.split("-")[0],16);
            bId = parseInt(b.privateUuid.split("-")[0],16);
            return bId - aId;
          }
        });
      }
      msg.message.stats = timer.getStats();
      return msg;
    }
  }
}();


var play = function() {
  return {
    setNumber: function(privateUuid, number) {
      var playerFound = false,
          numberActivated = false,
          repeatedChars = false
          status  = 0,
          message = {};

      var player = players.get(privateUuid);

      for(var x=0; x<number.toString().length && !repeatedChars; x++) {
        if(number.toString().indexOf(number.toString().charAt(x),x+1) >= 0) {
          repeatedChars = true;
        }
      }

      if(player != null) {
        playerFound = true;

        player.requests.quantity++;
        player.requests.last = timer.get();

        if(player.activeNumber === false) {
          if(!repeatedChars) {
            player.number = number;
            player.activeNumber = true;

            var numberIdHash = crypto.createHash('md5');
            numberIdHash.update(number.toString() + secret.toString());

            player.numberId = numberIdHash.digest('hex');
            numberActivated = true;
            scoreRules.setNumber(player);
          }
        } else {
          numberActivated = false;
        }
      }

      if(playerFound && numberActivated && !repeatedChars) {
        message = {number: number};
        status = 200;
      } else if(!playerFound){
        status = "521";
        message = {error: status, message: errorCodes[status]};
      } else  if(repeatedChars){
        status = "528";
        message = {error: status, message: errorCodes[status]};
        scoreRules.wasteRequest(player);
      } else {
        status = "523";
        message = {error: status, message: errorCodes[status]};
        scoreRules.wasteRequest(player);       
      }

      return {
        status: status,
        message: message
      }
    },
    guessNumber: function(privateUuid, oponentPublicUuid, number) {
      var playerFound = false,
          allowedToTry = false,
          timeToNextAttemp = 0,
          oponentFound = false,
          numberActivated = false,
          oponentNumberActivated = false,
          guessingOwnNumber = false,
          correctChars = 0,
          existingChars = 0,
          wrongChars = 0,
          status  = 0,
          message = {};

      var player = players.get(privateUuid);
      if(player != null) {
        playerFound = true;

        player.requests.quantity++;
        player.requests.last = timer.get();

        numberActivated = player.activeNumber;
        timeToNextAttemp = timer.timeToNextAttemp(player.lastGuessAttemp, options.minRetryTime);
        if(timeToNextAttemp <= 0) allowedToTry = true;
      }

      if(playerFound && player.publicUuid == oponentPublicUuid) {
        guessingOwnNumber = true;
      }

      var oponent = players.get(oponentPublicUuid);
      if(oponent != null) {
        oponentFound = true;
        oponentNumberActivated = oponent.activeNumber;
      }

      if(playerFound && allowedToTry && numberActivated && !guessingOwnNumber && oponentFound && oponentNumberActivated) {
        for(var x=0; x<number.toString().length; x++) {
          if(number.toString().charAt(x) == oponent.number.toString().charAt(x)) {
            correctChars++;
          } else if(oponent.number.toString().indexOf(number.toString().charAt(x)) >= 0) {
            existingChars++;
          } else {
            wrongChars++;
          }
        }

        if(correctChars === number.toString().length) {
          oponent.activeNumber = false;
          scoreRules.guessNumber(player, oponent, true);
        } else {
          scoreRules.guessNumber(player, oponent, false);
        }

        player.lastGuessAttemp = timer.get();

        message = {number: number, numberId: oponent.numberId, correctChars: correctChars, existingChars: existingChars, wrongChars: wrongChars, timeToNextAttemp: timer.timeToNextAttemp(player.lastGuessAttemp, options.minRetryTime)};
        status = 200;
      } else if(!playerFound) {
        status = "521";
        message = {error: status, message: errorCodes[status]};
        scoreRules.wasteRequest(player);
      } else if(!allowedToTry) {
        status = "524";
        message = {error: status, message: errorCodes[status], minInterval: options.minRetryTime, timeToNextAttemp: timeToNextAttemp};
        scoreRules.wasteRequest(player);
      } else if(!numberActivated) {
        status = "525";
        message = {error: status, message: errorCodes[status]};
        scoreRules.wasteRequest(player);        
      } else if(guessingOwnNumber) {
        status = "529";
        message = {error: status, message: errorCodes[status]};
        scoreRules.guessOwnNumber(player);        
      } else if(!oponentFound) {
        status = "526";
        message = {error: status, message: errorCodes[status]};
        scoreRules.wasteRequest(player);     
      } else {
        status = "527";
        message = {error: status, message: errorCodes[status]};
        scoreRules.wasteRequest(player);           
      }

      return {
        status: status,
        message: message
      }
    }
  }
}();

var scoreRules = function() {
  var failedAttemp = -5,
      sucessAttemp = +200,
      setNumber = -25,
      receivedFailedAttemp = +1,
      wasteRequest = -5,
      ownNumberAttemp = wasteRequest * 100;
  return {
    guessNumber: function(player, oponent, found) {
      if(player != null) {
        if(found) {
          player.score += sucessAttemp;
          player.stats.guessedNumbers.quantity++;
          player.stats.guessedNumbers.score += sucessAttemp;
        } else {
          player.score += failedAttemp;
          player.stats.wrongGuessedNumbers.quantity++;
          player.stats.wrongGuessedNumbers.score += failedAttemp;

          oponent.score += receivedFailedAttemp;
          oponent.stats.wrongGuessNumbersReceived.quantity++;
          oponent.stats.wrongGuessNumbersReceived.score += receivedFailedAttemp;
        }        
      }
    },
    guessOwnNumber: function(player) {
      if(player != null) {
          player.score += ownNumberAttemp;
          player.stats.wastedRequests.quantity++;
          player.stats.wastedRequests.score += ownNumberAttemp;
      }
    },
    setNumber: function(player) {
      if(player != null) {
        player.score += setNumber;
        player.stats.setedNumbers.quantity++;
        player.stats.setedNumbers.score += setNumber;
      }
    },
    wasteRequest: function(player) {
      if(player != null) {      
        player.score += wasteRequest;
        player.stats.wastedRequests.quantity++;
        player.stats.wastedRequests.score += wasteRequest;
      }
    }
  }
}();

var doc = function() {
  var apiDocPath = "./docs/server-api.html";
  var getApi = function() {
    var msg = "";
    if(fs.statSync(apiDocPath).isFile()) {
      msg = fs.readFileSync(apiDocPath,"utf-8");
    }
    return {status:200, message: msg}
  }
  return {get:getApi};
}();

var version = function() {
  var getVersion = function() {
    return {status:200, message: {
      version:API_VERSION
    }}
  }
  return {get:getVersion};
}();

var createRouter = function () {
  var router = new (journey.Router)({
    strict: false,
    strictUrls: false,
    api: 'basic'
  });
  
  /**
  * PLAYERS
  **/
  router.path(/\/players/, function () {
    this.get().bind(function (res) {
      res.send(404, {}, { error: 'command not found' });
    });
    
    this.get(/\/board\/([a-f|0-9]{8}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{12})/).bind(function (res, privateUuid) {
      var response = players.board(privateUuid);
      res.send(response.status, {}, response.message);
    });

    this.get(/\/register\/([A-z|0-9]+)/).bind(function (res, name) {
      var userIp = this.request.connection.remoteAddress;
      var response = players.register(name, userIp);
      res.send(response.status, {}, response.message);
    });
  });

  /**
  * GAME
  **/
  router.path(/\/play/, function () {
    this.get().bind(function (res) {
      res.send(404, {}, { action: 'command not found' });
    });
    
    this.get(/\/setnumber\/([a-f|0-9]{8}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{12})\/([0-9]{4})/).bind(function (res, privateUuid, number) {
      var response = play.setNumber(privateUuid, number);
      res.send(response.status, {}, response.message);
    });

    this.get(/\/guessnumber\/([a-f|0-9]{8}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{12})\/([a-f|0-9]{8}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{4}[\-][a-f|0-9]{12})\/([0-9]{4})/).bind(function (res, privateUuid, oponentPublicUuid, number) {
      var response = play.guessNumber(privateUuid, oponentPublicUuid, number);
      res.send(response.status, {}, response.message);
    });
  });
  /**
  * STATS
  **/
  router.path(/\/stats/, function () {
    this.get().bind(function (res) {
      res.send(200, {}, timer.getSlots());
    });
  });  

  /**
  * ADMIN
  **/
  router.path(/\/admin/, function () {
    this.get().bind(function (res) {
      res.send(404, {}, { action: 'command not found' });
    });
    
    this.get(/\/reset\/([A-z|0-9]+)/).bind(function (res, adminPassword) {
      var response = admin.reset(adminPassword);
      res.send(response.status, {}, response.message);
    });

    this.get(/\/board\/([A-z|0-9]+)/).bind(function (res, adminPassword) {
      var response = admin.board(adminPassword);
      res.send(response.status, {}, response.message);
    });
  });

  /**
  * DOC
  **/
  router.path(/\/doc/, function () {
    this.get().bind(function (res) {
      var response = doc.get();
      res.send(response.status, {}, response.message);
    });
  });

  /**
  * VERSIOM
  **/
  router.path(/\/version/, function () {
    this.get().bind(function (res) {
      var response = version.get();
      res.send(response.status, {}, response.message);
    });
  });


  return router;
};



exports.createServer = function (_options) {
  for(var option in _options) {
    options[option] = _options[option];
  }

  winston.info('Starting Server');
  var router = createRouter();
  
  var server = http.createServer(function (request, response) {
    var body = '';
    
    winston.info('Incoming Request', { url: request.url });
    timer.register();
    
    request.on('data', function (chunk) {
      body += chunk;
    });
    
    request.on('end', function () {
      router.handle(request, body, function (route) {
        route.headers["Access-Control-Allow-Origin"] = "*";
        route.headers["Cache-Control"] = "no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0";
        route.headers["Pragma"] = "no-cache";
        route.headers["Expires"] = "Thu, 01 Jan 1970 00:00:00 GMT";

        response.writeHead(route.status, route.headers);
        response.end(route.body);
      });
    })
  });
  
  exports.server = server;

  if (options.port) {
    server.listen(options.port);
  }
  
  return server;
};

exports.players = players;
exports.play = play;

